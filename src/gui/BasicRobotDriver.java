package gui;

import generation.CardinalDirection;
import generation.Distance;
import generation.MazeConfiguration;
import gui.Robot.Turn;

public abstract class BasicRobotDriver implements RobotDriver{
	Robot robot;
	int width;
	int height;
	Distance distance;
	float startBattery;
	
	public void finishMaze() {
		/**
		 * What wall is exit on?
		 * Rotate to exit
		 * Move forward 1
		 * check each edge
		 * 
		 * Assume the robot is right next to the exit, now needs to exit the maze
		 * First must find the exit.  Scan the left and right walls and then scan the top and bottom walls
		 * once the distance
		 */
		int[] position;
		CardinalDirection cardD;
		
		MazeConfiguration mazeConfig = ((BasicRobot) robot).getMaze().getMazeConfiguration();
		try {
			position = robot.getCurrentPosition();
		} catch(Exception e) {
			e.printStackTrace();
			return;
		}
		/**
		 * Scan the west and east wall for an exit
		 * Scan north and south wall for an exit
		 */
		cardD = getCardinalDirection(position, mazeConfig);
		
		/**
		 * Once the direction of the exit is found, robot either moves forward 1 if the exit is in front, 
		 * or turns towards the exit and moves forward 1
		 */
		moveRobot(cardD);
	}

	private void moveRobot(CardinalDirection cardD) {
		if (robot.getCurrentDirection() == cardD) {
			robot.move(1, false);
		}
		else if (robot.getCurrentDirection().rotateClockwise().oppositeDirection() == cardD) {
			robot.rotate(Turn.RIGHT);
			robot.move(1, false);
		}
		else if (robot.getCurrentDirection().rotateClockwise() == cardD) {
			robot.rotate(Turn.LEFT);
			robot.move(1, false);
		}
		else {
			robot.move(-1, false);
		}
	}

	private CardinalDirection getCardinalDirection(int[] position, MazeConfiguration mazeConfig) {
		CardinalDirection cardD;
		if(position[0] == 0 && !mazeConfig.hasWall(position[0], position[1], CardinalDirection.West)) {
			cardD = CardinalDirection.West;
		}
		else if (position[0] == mazeConfig.getWidth()-1 && !mazeConfig.hasWall(position[0], position[1], CardinalDirection.East) ) {
			cardD = CardinalDirection.East;
		}
		
		else if (position[1] == 0 && !mazeConfig.hasWall(position[0], position[1], CardinalDirection.North)) {
			cardD = CardinalDirection.North;
		}
		else {
			cardD = CardinalDirection.South;
		}
		return cardD;
	}
	
	/**
	 * Set up the robot
	 */
	@Override
	public void setRobot(Robot r) {
		robot = r;
		startBattery = r.getBatteryLevel();
	}
	/**
	 * Sets the dimensions of the maze
	 */
	@Override
	public void setDimensions(int width, int height) {
		this.width = width;
		this.height = height;
	}

	@Override
	public void setDistance(Distance distance) {
		this.distance = distance;
	}
	/**
	 * returns the get energy consumption function
	 */
	@Override
	public float getEnergyConsumption() {
		return startBattery - robot.getBatteryLevel();
	}

		/**
		 * Return Robots Odometer
		 */
	@Override
	public int getPathLength() {
		return robot.getOdometerReading();
	}
}
