package gui;

import generation.Distance;
import gui.Constants.UserInput;
import gui.Robot.Turn;

/**
 * 
 * @author Jamie Fletcher
 * Extends RobotDriver
 *Methods:
 * -setRobot sets up the robot (pretty much instantiates it)
 * -setDimenstions sets the dimensions of the maze
 * -setDistance sets the distance
 * -drive2Exit returns false
 * -getEnergyConsumption returns the energy consumption of the robot
 * -getPathLength return the robots odometer reading
 * -keyDown sets up the robot so that it can controll the driver
 */

public class ManualDriver extends BasicRobotDriver{

	@Override
	public boolean drive2Exit() throws Exception {
		return false;
	}
	
	public void keyDown(UserInput key) {
		switch (key) {
		case Up: // move forward 
			System.out.println("Move forward"); 
            robot.move(1, true);  
            break;
        case Left: // turn left
            robot.rotate(Turn.LEFT);
            break;
        case Right: // turn right
            robot.rotate(Turn.RIGHT);
            break;
        case Down: // move backward
            robot.move(-1, true);
            break;  
		}
	}

}
