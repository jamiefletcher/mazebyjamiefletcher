package generation;

import java.util.ArrayList;

public class MazeBuilderEller extends MazeBuilder implements Runnable {
	int paths[];
	int counter = 1;
	int nextPaths[];
	
	public MazeBuilderEller() {
		super();
		System.out.println("MazeBuilderPrim uses Eller's algorithm to generate maze.");
	}
	
	public MazeBuilderEller(boolean det) {
		super(det);
		System.out.println("MazeBuilderPrim uses Eller's algorithm to generate maze.");
	}
	
	@Override
	//Algotirthm for Eller
	protected void generatePathways() {
		setUp();
		for (int i = 0; i<height -1; i++) {
			oneRow(i);
		}
		for (int i = 0; i< width-1; i++) {
			if (paths[i] != paths[i+1]) {
				mergeCellH(height-1, i);
			}
		}
	}
	//Function that can be  called ot merge cells horizontally
	private void mergeCellH(int row, int column) {
		int check = paths[column + 1];
		for (int i = 0; i < width; i++) {
			if (paths[i] == check){
				paths [i] = paths[column];
			}
		}
		Wall wall = new Wall(column, row, CardinalDirection.East);
		cells.deleteWall(wall);
		
	}
	//Function that can be called to merge cells vertically
	private void mergeCellV(int row, int column) {
		nextPaths[column] = paths[column];
		Wall wall = new Wall(column, row, CardinalDirection.South);
		cells.deleteWall(wall);
	}
	//Function that deals with one row at a time
	private void oneRow(int row) {
		//Horizontal
		for (int column = 0; column<width-1; column++) {
			int rand = random.nextIntWithinInterval(0, 1);
			if (rand == 0 && paths[column] != paths[column+1]) {
				mergeCellH(row, column);
			}
		}
		//Verticle
		ArrayList<Integer> setList = new ArrayList<Integer>();
		for (int i = 0; i< width; i++) {
			if (!setList.contains(paths[i])) {
				setList.add(paths[i]);
			}
		}
		ArrayList<Integer> contains;
		for (int set: setList) {
			contains = new ArrayList<Integer>();
			for (int i = 0; i< width; i++) {
				if (paths[i] == set) {
					contains.add(i);
				}
			}
			int randomv = random.nextIntWithinInterval(0, contains.size()-1);
			int column = contains.get(randomv);
			mergeCellV(row, column);
		}
		for (int column = 0; column<width; column++) {
			int rand = random.nextIntWithinInterval(0, 1);
			if (rand == 0) {
				mergeCellV(row, column);
			}
		}
		
		for (int i = 0; i<width; i++) {
			if (nextPaths[i] == 0) {
				nextPaths[i] = counter;
				counter++;
			}
		}
		paths = nextPaths;
		nextPaths = new int[width];
	}
	//Sets up the array structure for the maze
	private void setUp() {
		//set up Array row
		paths = new int[width];
		for(int i = 1; i <= width; i++) {
			paths[i-1] = i;
			counter++;
		}
		nextPaths = new int[width];
	}
}
