package gui;

import generation.CardinalDirection;
import generation.MazeConfiguration;
import gui.Robot.Direction;
import gui.Robot.Turn;

public class Explorer extends BasicRobotDriver{

	@Override
	public boolean drive2Exit() throws Exception {
		//Instantiates all of the variables needed in the algorithm
		CardinalDirection cardDir;
		int cellCount = 0;
		boolean cellVisited;
		
		//While the robot is not at an exit, the algorithm will run as follows:
		while (!robot.isAtExit()) {
			if (robot.hasStopped()) {
				throw new Exception ("Explorer has failed");
			}
			/**
			 * Robot is outside a room
			 */
			if (!robot.isInsideRoom()) {
				/**
				 * Cases for not random decision
				 */
				if ((robot.distanceToObstacle(Direction.LEFT) == 0) && (robot.distanceToObstacle(Direction.RIGHT) == 0) && (robot.distanceToObstacle(Direction.FORWARD) > 0)) {
					robot.move(1, false);
					cellVisited = true;
				}
				else if ((robot.distanceToObstacle(Direction.LEFT) == 0) && (robot.distanceToObstacle(Direction.FORWARD) == 0) && (robot.distanceToObstacle(Direction.RIGHT) > 0)){
					robot.rotate(Turn.RIGHT);
					robot.move(1, false);
					cellVisited = true;
				}
				else if ((robot.distanceToObstacle(Direction.RIGHT) == 0) && (robot.distanceToObstacle(Direction.FORWARD) == 0) && (robot.distanceToObstacle(Direction.LEFT) > 0)) {
					robot.rotate(Turn.LEFT);
					robot.move(1, false);
				}
				else {
					robot.rotate(Turn.RIGHT);
					robot.move(1, false);
				}
			}
			/**
			 * Robot is inside a room
			 */
			else {
				/**
				 * Uses the WallFollower algorithm to find the exit of a room
				 */
				if (robot.distanceToObstacle(Direction.LEFT) > 0) {
					robot.rotate(Turn.LEFT);
					robot.move(1, false);
				}
				else if (robot.distanceToObstacle(Direction.FORWARD) > 0) {
					robot.move(1, false);
				}
				else {
					robot.rotate(Turn.RIGHT);
				}
			}
		}
		/**
		 * When the robot is at the exit it will return
		 */
		return robot.isAtExit();
	}

}
