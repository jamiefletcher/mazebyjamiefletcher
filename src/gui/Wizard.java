package gui;

import generation.CardinalDirection;
import generation.MazeConfiguration;
import gui.Robot.Turn;

public class Wizard extends BasicRobotDriver{

	@Override
	public boolean drive2Exit() throws Exception {
		/**
		 * sets up mazeconfig
		 * Creates 2 int variables: position, nextPosition to keep track of the current position and the next position
		 * Imports CardinalDirection so that it can bhe used to rotate the robot
		 */
		MazeConfiguration mazeConfig = ((BasicRobot) robot).getMaze().getMazeConfiguration();
		
		/**
		 * Starts the Wizard algorithm
		 */
		while (!robot.isAtExit() ) {
			getNextDirection(mazeConfig);
		}
		
		/**
		 * Once the robot is at an exit it exits the maze
		 */
		return robot.isAtExit();
	}

	/**
	 * This function gets the next direction that robot will move
	 * Also throws an exception if the Wizard algorithm has failed
	 * @param mazeConfig
	 * @throws Exception
	 */
	private void getNextDirection(MazeConfiguration mazeConfig) throws Exception {
		CardinalDirection cardDir = getCurrentDirection(mazeConfig);
		move(cardDir);
		/**
		 * Check if robot still has energy or hit a wall
		 */
		if (robot.hasStopped()) {
			throw new Exception ("Wizard has failed");
		}
	}
	
	/**
	 * Takes the cardinaldirection info from above and rotates the robot to the direction of the next 
	 * position and moves it forward 1 cell
	 * @param cardDir
	 */
	private void move(CardinalDirection cardDir){
		if (robot.getCurrentDirection() == cardDir) {
			robot.move(1, false);
		}
		else if (robot.getCurrentDirection().rotateClockwise().oppositeDirection() == cardDir) {
			robot.rotate(Turn.RIGHT);
			robot.move(1, false);
		}
		else if (robot.getCurrentDirection().rotateClockwise() == cardDir) {
			robot.rotate(Turn.LEFT);
			robot.move(1, false);
		}
		else {
			robot.move(-1, false);
		}
	}

	/**
	 * returns the cardinal direction of the next position in relation to the robots current position
	 */
	private CardinalDirection getCurrentDirection(MazeConfiguration mazeConfig) throws Exception {
		int[] position;
		int[] nextPosition;
		CardinalDirection cardDir;
		
		position = robot.getCurrentPosition();
		nextPosition = mazeConfig.getNeighborCloserToExit(position[0], position[1]);
		if (nextPosition[0] > position[0]) {
			cardDir = CardinalDirection.East;
		}
		else if (nextPosition[0] < position[0]) {
			cardDir = CardinalDirection.West;
		}
		else if (nextPosition[1] > position[1]) {
			cardDir = CardinalDirection.South;
		}
		else  {
			cardDir = CardinalDirection.North;
		}
		return cardDir; 
	}

}
