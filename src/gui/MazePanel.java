package gui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Color; 
import java.awt.Panel;
import java.awt.RenderingHints; 
import generation.Seg;

/**
 * Add functionality for double buffering to an AWT Panel class.
 * Used for drawing a maze.
 * 
 * @author Peter Kemper
 *
 */
public class MazePanel extends Panel {
	/* Panel operates a double buffer see
	 * http://www.codeproject.com/Articles/2136/Double-buffer-in-standard-Java-AWT
	 * for details
	 */
	// bufferImage can only be initialized if the container is displayable,
	// uses a delayed initialization and relies on client class to call initBufferImage()
	// before first use
	private Image bufferImage;  
	private Graphics2D graphics; // obtained from bufferImage, 
	// graphics is stored to allow clients to draw on the same graphics object repeatedly
	// has benefits if color settings should be remembered for subsequent drawing operations 
	/**
	 * Constructor. Object is not focusable.
	 */
	public MazePanel() {
		setFocusable(false);
		bufferImage = null; // bufferImage initialized separately and later
		graphics = null;	// same for graphics
	}
	
	@Override
	public void update(Graphics g) {
		paint(g);
	}
	/**
	 * Method to draw the buffer image on a graphics object that is
	 * obtained from the superclass. 
	 * Warning: do not override getGraphics() or drawing might fail. 
	 */
	public void update() {
		paint(getGraphics());
	}
	
	/**
	 * Draws the buffer image to the given graphics object.
	 * This method is called when this panel should redraw itself.
	 * The given graphics object is the one that actually shows 
	 * on the screen.
	 */
	@Override
	public void paint(Graphics g) {
		if (null == g) {
			System.out.println("MazePanel.paint: no graphics object, skipping drawImage operation");
		}
		else {
			g.drawImage(bufferImage,0,0,null);	
		}
	}

	/**
	 * Obtains a graphics object that can be used for drawing.
	 * This MazePanel object internally stores the graphics object 
	 * and will return the same graphics object over multiple method calls. 
	 * The graphics object acts like a notepad where all clients draw 
	 * on to store their contribution to the overall image that is to be
	 * delivered later.
	 * To make the drawing visible on screen, one needs to trigger 
	 * a call of the paint method, which happens 
	 * when calling the update method. 
	 * @return graphics object to draw on, null if impossible to obtain image
	 */
	public Graphics getBufferGraphics() {
		// if necessary instantiate and store a graphics object for later use
		if (null == graphics) { 
			if (null == bufferImage) {
				bufferImage = createImage(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
				if (null == bufferImage)
				{
					System.out.println("Error: creation of buffered image failed, presumedly container not displayable");
					return null; // still no buffer image, give up
				}		
			}
			graphics = (Graphics2D) bufferImage.getGraphics();
			if (null == graphics) {
				System.out.println("Error: creation of graphics for buffered image failed, presumedly container not displayable");
			}
			else {
				// System.out.println("MazePanel: Using Rendering Hint");
				// For drawing in FirstPersonDrawer, setting rendering hint
				// became necessary when lines of polygons 
				// that were not horizontal or vertical looked ragged
				graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
						RenderingHints.VALUE_ANTIALIAS_ON);
				graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
						RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			}
		}
		return graphics;
	} 
	
	/**
	 * Fills polygon with the awt graphics so that it is only called inside of 
	 * MazePanel.java and not in any other classes for easy updating when switching 
	 * to Android. 
	 * @param x
	 * @param y
	 * @param points 
	 * @author Jamie Fletcher and Jack Liegey 
	 */
	public void fillPolygon(int[] x, int[] y, int points) {  
		graphics.fillPolygon(x, y, points); 
	}
	
	//enum values for different color choices to be selected
	public enum ColorChoice { WHITE, BLACK, DARKGRAY, GRAY, YELLOW, RED }; 
	
	/**
	 * Allows for classes outside of MazePanel.java to select a color and set it without 
	 * having to reference the Java awt graphics. For easy switch over to Android when the 
	 * time comes. 
	 * @param color 
	 * @author Jamie Fletcher and Jack Liegey 
	 */
	public void color(ColorChoice color) { 
		Graphics g = this.getBufferGraphics();
		
		switch(color) { 
		case WHITE: 
			g.setColor(Color.white);
			break;
			
		case BLACK: 
			g.setColor(Color.black);
			break;
			
		case DARKGRAY: 
			g.setColor(Color.darkGray);
			break; 
			
		case GRAY: 
			g.setColor(Color.gray);
			break; 
			
		case YELLOW: 
			g.setColor(Color.yellow);
			break; 
			
		case RED: 
			g.setColor(Color.red);
			break;
			
		}
		
	} 
	
	/**
	 * sets the color using the different integers for red, blue, and green, when they are set randomly 
	 * based on the switch statement in Seg.java. Allows for easy updating when switching over to Android. 
	 * @param r
	 * @param g
	 * @param b 
	 * @author Jamie Fletcher and Jack Liegey 
	 */
	public void setColor(int r, int g, int b) { 
		graphics.setColor(new Color(r, g, b));
	} 
	
	/** 
	 * sets the color using the single integer for the specific color like how it is called in 
	 * MazeFileReader.java. Just another way to set the color based on the different methods used, and 
	 * allows for awt graphics to only be called here in MazePanel.java, but the color can still be set 
	 * in the other classes. Allows for easier updating when switching ovet to Android. 
	 * @param colorInt 
	 * @author Jamie Fletcher Jack Liegey 
	 */
	public void setColor(int colorInt) { 
		graphics.setColor(new Color(colorInt));
	}
	
	/** 
	 * Gets the RGB value of the specified color and returns it to the class that needs it. 
	 * @param r
	 * @param g
	 * @param b
	 * @return RGB color value 
	 * @author Jamie Fletcher Jack Liegey 
	 */
	public static int getRBG(int r, int g, int b) { 
		return new Color(r, g, b).getRGB();  
	} 
	
	/** 
	 * returns the separate color values for the red, blue, and green specific settings to allow 
	 * for easier use when all three values are needed.  
	 * @param colorInt
	 * @return array of three separate color values. 
	 * @author Jamie Fletcher and Jack Liegey 
	 */
	public static int[] getRGBArray(int colorInt) { 
		Color color = new Color(colorInt); 
		return new int[] {color.getRed(), color.getGreen(), color.getBlue()}; 
	}
	
	/** 
	 * Fills a rectangle with the awt graphics so that it is only called inside of 
	 * MazePanel.java and not in any other classes for easy updating when switching 
	 * to Android.
	 * @param x
	 * @param y
	 * @param width
	 * @param height 
	 * @author Jamie Fletcher and Jack Liegey
	 */
	public void fillRectangle(int x, int y, int width, int height) { 
		Graphics g = this.getBufferGraphics();
		g.fillRect(x, y, width, height);
	}  
	
	/** 
	 * Draws a line with the awt graphics so that it is only called inside of 
	 * MazePanel.java and not in any other classes for easy updating when switching 
	 * to Android.
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2 
	 * @author Jamie Fletcher and Jack Liegey
	 */
	public void drawLine(int x1, int y1, int x2, int y2) { 
		Graphics g = this.getBufferGraphics(); 
		g.drawLine(x1, y1, x2, y2); 
	} 
	
	/** 
	 * Fills an oval with the awt graphics so that it is only called inside of 
	 * MazePanel.java and not in any other classes for easy updating when switching 
	 * to Android.
	 * @param x
	 * @param y
	 * @param width
	 * @param height 
	 * @author Jamie Fletcher and Jack Liegey
	 */
	public void fillOval(int x, int y, int width, int height) { 
		Graphics g = this.getBufferGraphics(); 
		g.fillOval(x, y, width, height); 
	}
	

}
