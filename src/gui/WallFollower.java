package gui;

import gui.Robot.Direction;
import gui.Robot.Turn;

public class WallFollower extends BasicRobotDriver{

	/**
	 * This is the algorithm for WallFollower
	 */
	@Override
	public boolean drive2Exit() throws Exception {
		/**
		 * While the robot is not at an exit
		 */
		while(!robot.isAtExit()) {
			followWall();
		}
		/**
		 * robot is next to exit
		 */
		return robot.isAtExit();
	}

	private void followWall() throws Exception {
		/**
		 * if the robot has stopped throw an error
		 */
		if (robot.hasStopped()) {
			throw new Exception ("Robot has failed");
		}
		/**
		 * If the distance to wall on the left > zero, turn towards it, and then move forward 1
		 */
		if (robot.distanceToObstacle(Direction.LEFT) > 0) {
			robot.rotate(Turn.LEFT);
			robot.move(1, false);
		}
		/**
		 * if the distance to the obstacle in front of the robot is > 0, move forward 1
		 */
		else if (robot.distanceToObstacle(Direction.FORWARD) > 0) {
			robot.move(1, false);
		}
		/**
		 * if the distance to the obstacle to the front and left == 0, turn right
		 */
		else {
			robot.rotate(Turn.RIGHT);
		}
	}

}
