package gui;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import generation.Factory;
import generation.MazeFactory;
import generation.Order;
import generation.Order.Builder;


class BasicRobotTest {
	StateGenerating gen;
	
	@Before
	public void setup() {
		gen = new StateGenerating();
		Builder builder = Order.Builder.DFS;
		Controller control;
		
		gen.setSkillLevel(1);
		gen.setBuilder(builder);
		gen.setPerfect(true);
		
		Factory factory = new MazeFactory();
		factory.order(gen);
		factory.waitTillDelivered();
	}
	
	@Test
	void pathLength() {
		/**
		 * Test to make sure that the odometer at the end is the correct path length
		 */
		fail("Not yet implemented");
	}

	@Test
	void batteryDistanceSensing() {
		/**
		 * Test to make sure that the battery consumption for for DistanceSensing is correct
		 */
	}
	
	@Test
	void batteryRotating() {
		/**
		 * Test to make sure that the battery consumption for one rotation subtracts 3 from battery level
		 */
	}
	
	@Test
	void batteryMovingForward() {
		/**
		 * Test to make sure that the battery consumption for for one move forward subtracts 5 from battery level
		 */
	}
	
	@Test
	void batteryMovingBackward() {
		/**
		 * Test to make sure that the battery consumption for for one moving backward subtracts 5 from battery level
		 */
	}
}
