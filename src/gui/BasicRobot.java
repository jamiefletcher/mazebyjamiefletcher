package gui;

import generation.CardinalDirection;
import generation.Cells;
import gui.Constants.UserInput;

/**
 * 
 * @author Jamie Fletcher
 * Extends Robot
 * Methods:
 * -Rotate function rotates the robot
 * -move function moves robot "distance" cell
 * -getCurrentPosition gets the robots current position in the maze (x,y) coordinates
 * -setMaze sets up the maze
 * -isAtExit returns true if the robot is at an exit
 * -canSeeExit returns true if the robot can see an exit directly in front of it
 * -isInsideRoom determines if the robot is inside of a room
 * -hasRoomSensor returns true
 * -CardinalDirection returns the current cardinal direction that the robot is facing
 * -getBatteryLevel returns the current battery level
 * -setBatteryLevel sets the battery to level
 * -getOdometerReading returns the odometer reading
 * -resetOdometer resets the odometer to 0
 * -getEnergyForFullRotation returns 12
 * -getEnergyForStepForward returns 5
 * -hasStopped returns stopped
 * -distanceToObstacle returns the distance to a wall in front of the robot
 * -hasDistanceSensor returns true
 *
 */
public class BasicRobot implements Robot {

	Controller maze;
	StatePlaying statePlaying;  
	int odometer = 0;
	float battery = 3000;
	boolean stopped = false;

	/**
	 * Allows robot to turn right, left, or around
	 */
	@Override
	public void rotate(Turn turn) {
		directionToRotate(turn);
	}

	private void directionToRotate(Turn turn) {
		if (battery < 3) {
			return;
		}
		if (turn == Turn.RIGHT) {
			statePlaying.rotate(-1);  
			battery = battery - 3;
		} else if (turn == Turn.LEFT) {
			statePlaying.rotate(1);  
			battery = battery - 3;
		} else if (turn == Turn.AROUND) {
			if (battery < 6) {
				return;
			}
			statePlaying.rotate(1);
			statePlaying.rotate(1);  
			battery = battery - 6;
		}
	}

	public Controller getMaze() {
		return maze;
	}
	/**
	 * method that allows the robot to move forward or backwards
	 */
	@Override
	public void move(int distance, boolean manual) {
		calculateDistanceToMove(distance, manual);
	}

	private void calculateDistanceToMove(int distance, boolean manual) {
		if (distance > 0) {
			for (int i = 0; i < distance; i++) {
				if (battery < 5) {
					return;
				}
				if (statePlaying.checkMove(1)) {
					statePlaying.walk(1); 
					battery = battery - 5;
					odometer++; 
				}
				else if (!manual) {
					stopped = true;
					return;
				}
			}
		}
		if (distance < 0) {
			for (int i = 0; i > distance; i--) {
				if (battery < 5) {
					return;
				}
				if (statePlaying.checkMove(-1)) {
					statePlaying.walk(-1);  
					battery = battery - 5;
					odometer++;
				}
				else if (!manual) {
					stopped = true;
					return;
				}
			}
		}
	}

	/**
	 * gets the current position of the robot. If the robot is outside of the maze
	 * it throws an exception
	 */
	@Override
	public int[] getCurrentPosition() throws Exception {

		int[] position = statePlaying.getCurrentPosition();
		if (!statePlaying.isOutside(position[0], position[1])) {
			return position;
		} else {
			throw new Exception("Position is outside of maze");
		}
	}

	@Override
	public void setMaze(Controller controller) {
		maze = controller;
		statePlaying = (StatePlaying) maze.states[2];
	}

	/**
	 * Determines if robot is at an exit
	 */
	@Override
	public boolean isAtExit() {
		try {
			int[] currentPosition = getCurrentPosition();
			if (maze.getMazeConfiguration().getDistanceToExit(currentPosition[0], currentPosition[1]) == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * If there is an exit in the forward direction in a straight line, return true,
	 * else it throws an error
	 */

	@Override
	public boolean canSeeExit(Direction direction) throws UnsupportedOperationException {
		if (distanceToObstacle(direction) == Integer.MAX_VALUE) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isInsideRoom() throws UnsupportedOperationException {
		if (!hasRoomSensor()) {
			throw new UnsupportedOperationException ("Has not room sensor");
		}
		return senseRoom();
		
	}

	private boolean senseRoom() {
		Cells cell = maze.getMazeConfiguration().getMazecells();
		int[] curPos = maze.getCurrentPosition();
		if (battery < 1) {
			return false;
		}
		battery = battery - 1;
		return cell.isInRoom(curPos[0], curPos[1]);
	}

	@Override
	public boolean hasRoomSensor() {
		return true;
	}

	@Override
	public CardinalDirection getCurrentDirection() {
		return maze.getCurrentDirection();
	}

	@Override
	public float getBatteryLevel() {
		return battery;
	}

	@Override
	public void setBatteryLevel(float level) {
		battery = level;
	}

	@Override
	public int getOdometerReading() {
		return odometer;
	}

	/**
	 * Resets the odometer to 0
	 */
	@Override
	public void resetOdometer() {
		odometer = 0;

	}

	@Override
	public float getEnergyForFullRotation() {
		return 12;
	}

	@Override
	public float getEnergyForStepForward() {
		return 5;
	}

	@Override
	public boolean hasStopped() {
		return stopped;
	}

	@Override
	public int distanceToObstacle(Direction direction) throws UnsupportedOperationException {
		return calculateDistance(direction);

	}

	private int calculateDistance(Direction direction) {
		int distance = 0;
		int[] currentPos;
		if (!hasDistanceSensor(direction)) {
			throw new UnsupportedOperationException ("There is not distance sensor");
		}
		// Variables:
		// Absolute Direction I'm currently facing
		// Absolute Direction that is being requested
		CardinalDirection facingDir = maze.getCurrentDirection();
		CardinalDirection actualDirection;

		if (direction == Direction.FORWARD) {
			actualDirection = facingDir;
		} else if (direction == Direction.BACKWARD) {
			actualDirection = facingDir.oppositeDirection();
		} else if (direction == Direction.LEFT) {
			actualDirection = facingDir.rotateClockwise();
		} else {
			actualDirection = facingDir.rotateClockwise().oppositeDirection();
		}
		try {
			currentPos = getCurrentPosition();
		} catch (Exception e) {
			return -1;
		}
		while (!maze.getMazeConfiguration().hasWall(currentPos[0], currentPos[1], actualDirection)) {
			distance++;
			currentPos[0] += actualDirection.getDirection()[0];
			currentPos[1] += actualDirection.getDirection()[1];
			if (!maze.getMazeConfiguration().isValidPosition(currentPos[0], currentPos[1])) {
				return Integer.MAX_VALUE;
			}
		}
		if (battery < 1) {
			return -1;
		}
		battery = battery - 1;
		return distance;
	}

	@Override
	public boolean hasDistanceSensor(Direction direction) {
		return true;
	}

}
